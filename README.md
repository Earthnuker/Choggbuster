# Choggbuster

Choggbuster is a set of python scripts aimed at automated preprocessing of video DVDs for archival and filtering

# Requirements

- python (obviously)
- libdvdcss (for decrypting copy protected DVDs)
- libdvdnav (for streaming the VOBs to disk)
- libdvdread (for reading decrypted data off of DVDs)
- ffmpeg (for demuxing)
- ccextractor (for extracting DVD Subtitles)

# Setup (Windows)

1. Clone the repo
2. `pip install cffi tqdm`
3. Grab [libdvdread, libdvdnav](https://www.videolan.org/developers/libdvdnav.html) and [libdvdcss](https://www.videolan.org/developers/libdvdcss.html) from VLC and drop them next to `dvd_ripper.py`
4. `python dvd_ripper.py F:\` or `python dvd_ripper.py D:\path\to\DVD.ISO`
5. this will create a folder `out` with a subfolder for the disc containing:
    - JSON file with metadata for the DVD title (`XXXX.json` where `X` is the title number)
    - demuxed streams (`tXXX_aYYY_Z_0xAAA.{ext}` where `X` is the title number, `Y` is the angle number `Z` is stream index and `AAA` is the stream id)
        - `.m2v` for video
        - `.ac3` or `.dtx` for audio
        - `.sub` and `.idx` for subtitles
        - `.srt` for captions
    - `.d2v` file for use with AviSynth and Vapoursynth D2V reader
    - `.info.json` file containing video stream metadata (cropping information, interlaced/progressive frame count and aspect ration information)
